package tn.iit.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import tn.iit.dto.MedicationDto;
import tn.iit.dto.TraitementDto;
import tn.iit.utils.Links;

import java.util.List;

@Service
public class MedicalFileClientService {

    private Logger logger = LoggerFactory.getLogger(MedicalFileClientService.class);
    private final RestTemplate restTemplate;

    public MedicalFileClientService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<TraitementDto> deleteAllTraitementsByMedicationId(long medicationId) {
        logger.debug("Deleting all Traitements contains the Medicaton {}",medicationId);
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(Links.TRAITEMENT + "/deleteByMedicationId")
                .build()
                .encode();
        List<TraitementDto> traitementDtos = restTemplate.exchange(uriComponents.toUri(),
                HttpMethod.POST,
                new HttpEntity<>(medicationId, new HttpHeaders()),
                new ParameterizedTypeReference<List<TraitementDto>>() {
                }).getBody();
        return traitementDtos;
    }

}
