package tn.iit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.dao.MedicationCategoryDao;
import tn.iit.dto.MedicationCategoryDto;
import tn.iit.factory.MedicationCategoryFactory;

import java.util.Collection;

@Transactional
@Service
public class MedicationCategoryService {

    private final MedicationCategoryDao medicationCategoryDao;

    @Autowired
    public MedicationCategoryService(MedicationCategoryDao medicationCategoryDao) {
        this.medicationCategoryDao = medicationCategoryDao;
    }

    @Transactional()
    public MedicationCategoryDto save(MedicationCategoryDto medicationCategoryDto) {
        this.medicationCategoryDao.saveAndFlush(MedicationCategoryFactory.categoryDtoToCategory(medicationCategoryDto));
        return medicationCategoryDto;
    }

    @Transactional()
    public void deleteById(Long id) {
        this.medicationCategoryDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public MedicationCategoryDto findOne(Long id) {
        return MedicationCategoryFactory.categoryToCategoryDto(this.medicationCategoryDao.getOne(id));
    }

    @Transactional(readOnly = true)
    public Collection<MedicationCategoryDto> findAll() {
        return MedicationCategoryFactory.categoriesToCategoriesDtos(this.medicationCategoryDao.findAll());
    }
}
