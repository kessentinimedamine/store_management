package tn.iit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.iit.dao.MedicationDao;
import tn.iit.dto.MedicationDto;
import tn.iit.entity.Medication;
import tn.iit.factory.MedicationFactory;
import tn.iit.web.rest.errors.ResourceNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class MedicationService {

    private final MedicationDao medicationDao;
    private final MedicalFileClientService medicalFileClientService;

    @Autowired
    public MedicationService(MedicationDao medicationDao, MedicalFileClientService medicalFileClientService) {
        this.medicationDao = medicationDao;
        this.medicalFileClientService = medicalFileClientService;
    }

    @Transactional()
    public MedicationDto save(MedicationDto medicationDto) {
        medicationDao.saveAndFlush(MedicationFactory.medicamentDtoToMedicament(medicationDto));
        return medicationDto;
    }


    @Transactional()
    public void deleteById(long id) {
        this.medicalFileClientService.deleteAllTraitementsByMedicationId(id);
        this.medicationDao.deleteById(id);
    }

    @Transactional(readOnly = true)
    public MedicationDto findOne(Long id) {
        Optional<Medication> medicament = this.medicationDao.findById(id);
        if (!medicament.isPresent()) {
            throw new ResourceNotFoundException("Medicament with id " + id + " not found");
        }
        return MedicationFactory.medicamentToMedicamentDto(medicament.get());
    }

    @Transactional(readOnly = true)
    public Collection<MedicationDto> findAll(Pageable pageable) {
        return MedicationFactory.medicamentsToMedicamentDtos(this.medicationDao.findAll(pageable).getContent());
    }

    @Transactional(readOnly = true)
    public MedicationDto findById(Long id) {
        Medication medication = this.medicationDao.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Medication with id " + id + " is not found !!"));
        return MedicationFactory.medicamentToMedicamentDto(medication);
    }

    @Transactional(readOnly = true)
    public Collection<MedicationDto> findManyByIds(List<Long> idList) {
        return MedicationFactory.medicamentsToMedicamentDtos(this.medicationDao.findAllById(idList));
    }

    public Collection<MedicationDto> findAllByMedicationCategoryId(long MedicationCategoryId) {
        return MedicationFactory.medicamentsToMedicamentDtos(this.medicationDao.findAllByCategory_Id(MedicationCategoryId));
    }
}
