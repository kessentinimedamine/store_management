package tn.iit.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.MedicationCategoryDto;
import tn.iit.service.MedicationCategoryService;
import tn.iit.service.MedicationService;

import javax.validation.Valid;
import java.util.Collection;

@CrossOrigin("*")
@RequestMapping(value = "/api/medication_category")
@RestController()
public class MedicationCategoryRessource {

    private final Logger logger = LoggerFactory.getLogger(MedicationCategoryRessource.class);
    private final MedicationCategoryService medicationCategoryService;
    private final MedicationService medicationService;

    public MedicationCategoryRessource(MedicationCategoryService medicationCategoryService, MedicationService medicationService) {
        this.medicationCategoryService = medicationCategoryService;
        this.medicationService = medicationService;
    }

    @GetMapping("{id}")
    public MedicationCategoryDto findOne(@PathVariable("id") long id) {
        this.logger.debug("Getting Medication Category {}", id);
        return this.medicationCategoryService.findOne(id);
    }

    @GetMapping
    public Collection<MedicationCategoryDto> findAll() {
        this.logger.debug("Getting all Medication Category");
        return this.medicationCategoryService.findAll();
    }

    @PostMapping()
    public MedicationCategoryDto add(@Valid @RequestBody MedicationCategoryDto categoryDto) {
        this.logger.debug("Adding new Medication Category {}", categoryDto.getName());
        return this.medicationCategoryService.save(categoryDto);
    }

    @PutMapping()
    public MedicationCategoryDto update(@Valid @RequestBody MedicationCategoryDto categoryDto) {
        this.logger.debug("Updating Medication Category {} with {}", categoryDto.getId(), categoryDto.getName());
        return this.medicationCategoryService.save(categoryDto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") long id) {

        this.logger.debug("Deleting All Medications with Medication Category {}", id);
        this.medicationService.findAllByMedicationCategoryId(id).forEach(medicationDto -> {
            this.logger.debug("Deleting Medication {} ", medicationDto.getId());
            this.medicationService.deleteById(medicationDto.getId());
        });

        this.logger.debug("Deleting Medication Category {}", id);
        this.medicationCategoryService.deleteById(id);
    }
}
