package tn.iit.web.rest.errors;


public class ErrorSM {

    private String message;
    private String conflict;

    public ErrorSM(String message, String conflict) {
        this.message = message;
        this.conflict = conflict;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getConflict() {
        return conflict;
    }

    public void setConflict(String conflict) {
        this.conflict = conflict;
    }

    @Override
    public String toString() {
        return "ErrorSM{" +
                "message='" + message + '\'' +
                ", conflict='" + conflict + '\'' +
                '}';
    }
}
