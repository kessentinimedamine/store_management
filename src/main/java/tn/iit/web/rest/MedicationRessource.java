package tn.iit.web.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import tn.iit.dto.MedicationDto;
import tn.iit.service.MedicationService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@CrossOrigin("*")
@RequestMapping(value = "/api/medications")
@RestController()
public class MedicationRessource {

    private final Logger logger = LoggerFactory.getLogger(MedicationRessource.class);
    private final MedicationService medicationService;

    public MedicationRessource(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping("/{id}")
    public MedicationDto findOne(@PathVariable("id") long id) {
        this.logger.debug("Getting Medication {}", id);
        return this.medicationService.findOne(id);
    }

    @GetMapping()
    public Collection<MedicationDto> findAll(
            @RequestParam(defaultValue = "0") int pageNum,
            @RequestParam(defaultValue = "4") int pageSize,
            @RequestParam(defaultValue = "id") String pageSort
    ) {
        this.logger.debug("Getting all Medications");
        return this.medicationService.findAll(PageRequest.of(pageNum, pageSize, Sort.by(pageSort).ascending()));
    }

    @PostMapping()
    public MedicationDto add(@Valid @RequestBody MedicationDto medicationDto) {
        this.logger.debug("Adding new Medication {}", medicationDto);

        return this.medicationService.save(medicationDto);
    }

    @PutMapping()
    public MedicationDto update(@Valid @RequestBody MedicationDto medicationDto) {
        this.logger.debug("Updating Medication {} with {}", medicationDto.getId(), medicationDto);
        return this.medicationService.save(medicationDto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") long id) {
        this.logger.debug("Deleting Medication {}", id);
        this.medicationService.deleteById(id);
    }

    @PostMapping("/searches")
    public Collection<MedicationDto> searches(@Valid @RequestBody List<Long> idList) {
        this.logger.debug("Getting all Medicaments with idList {}", idList);
        return this.medicationService.findManyByIds(idList);
    }

}
