package tn.iit.factory;

import tn.iit.dto.MedicationCategoryDto;
import tn.iit.dto.MedicationDto;
import tn.iit.entity.Medication;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class MedicationFactory {
    public static MedicationDto medicamentToMedicamentDto(Medication medicament) {
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setId(medicament.getId());
        medicationDto.setDosage(medicament.getDosage());
        medicationDto.setCategoryName(medicament.getCategory().getName());
        medicationDto.setCategoryId(medicament.getCategory().getId());
        medicationDto.setExpiredDate(medicament.getExpiredDate());
        medicationDto.setPrice(medicament.getPrice());
        medicationDto.setName(medicament.getName());
        return medicationDto;
    }

    public static Medication medicamentDtoToMedicament(MedicationDto medicationDto) {
        Medication medication = new Medication();
        medication.setId(medicationDto.getId());
        medication.setDosage(medicationDto.getDosage());
        MedicationCategoryDto categoryDto = new MedicationCategoryDto(medicationDto.getCategoryId());
        medication.setCategory(MedicationCategoryFactory.categoryDtoToCategory(categoryDto));
        medication.setExpiredDate(medicationDto.getExpiredDate());
        medication.setPrice(medicationDto.getPrice());
        medication.setName(medicationDto.getName());
        return medication;
    }

    public static Collection<MedicationDto> medicamentsToMedicamentDtos(Collection<Medication> medications) {
        List<MedicationDto> medicationDtoList = new ArrayList<>();
        medications.forEach(medication -> {
            medicationDtoList.add(medicamentToMedicamentDto(medication));
        });
        return medicationDtoList;
    }

}
