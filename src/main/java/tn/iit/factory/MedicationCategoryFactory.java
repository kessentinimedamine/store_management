package tn.iit.factory;


import tn.iit.dto.MedicationCategoryDto;
import tn.iit.entity.MedicationCategory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MedicationCategoryFactory {

    public static MedicationCategoryDto categoryToCategoryDto(MedicationCategory category) {
        return new MedicationCategoryDto(category.getId(), category.getName());
    }

    public static MedicationCategory categoryDtoToCategory(MedicationCategoryDto categoryDto) {
        MedicationCategory category = new MedicationCategory();
        category.setId(categoryDto.getId());
        category.setName(categoryDto.getName());
        return category;
    }

    public static Collection<MedicationCategoryDto> categoriesToCategoriesDtos(Collection<MedicationCategory> categories) {
        List<MedicationCategoryDto> categoryDtoList = new ArrayList<MedicationCategoryDto>();
        categories.forEach(category -> {
            categoryDtoList.add(categoryToCategoryDto(category));
        });
        return categoryDtoList;
    }
}
