package tn.iit.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.Medication;

import java.util.Collection;

@Repository
public interface MedicationDao extends JpaRepository<Medication, Long> {

    Collection<Medication> findAllByCategory_Id(long category_id);
}
