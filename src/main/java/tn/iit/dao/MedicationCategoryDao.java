package tn.iit.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.iit.entity.MedicationCategory;

@Repository
public interface MedicationCategoryDao extends JpaRepository<MedicationCategory, Long> {
}
