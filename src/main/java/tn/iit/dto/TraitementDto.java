package tn.iit.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class TraitementDto {

    private long id;
    @NotNull
    private long dossierId;
    @NotNull
    private long medicationId;

    public TraitementDto() {
    }

    public TraitementDto(long id, @NotNull long dossierId, @NotNull long medicationId) {
        this.id = id;
        this.dossierId = dossierId;
        this.medicationId = medicationId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDossierId() {
        return dossierId;
    }

    public void setDossierId(long dossierId) {
        this.dossierId = dossierId;
    }

    public long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(long medicationId) {
        this.medicationId = medicationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TraitementDto)) return false;

        TraitementDto that = (TraitementDto) o;

        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "TraitementDto{" +
                "id=" + id +
                ", dossierId=" + dossierId +
                ", medicationId=" + medicationId +
                '}';
    }
}

