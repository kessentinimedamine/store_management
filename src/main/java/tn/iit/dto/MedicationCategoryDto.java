package tn.iit.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class MedicationCategoryDto {

    private long id;

    @NotNull
    @NotEmpty
    @Size(min = 3)
    private String name;

    public MedicationCategoryDto() {
    }

    public MedicationCategoryDto(long id) {
        this.id = id;
    }

    public MedicationCategoryDto(long id, @NotNull @NotEmpty @Size(min = 3) String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MedicationCategoryDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
